FROM ruby:2.2.4

RUN apt-get update
RUN apt-get install rsync -y
RUN mkdir /cookbook
WORKDIR /cookbook

# Copy over the Gemfile and run bundle install.
# This is done as a separate steps so the image can be cached
# this step won't be rerun unless you change the Gemfile or
# Gemfile.lock
COPY Gemfile Gemfile.lock /cookbook/
RUN bundle install --jobs 20 --retry 5

# Copy the complete application onto the container
COPY . /cookbook/
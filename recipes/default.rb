apt_update 'Update the apt cache daily' do
  frequency 86_400
  action :periodic
end

# install common packages
%w(curl git htop unzip zip).each do |pkg|
  package pkg
end

describe package('curl') do
  it { should be_installed }
  # its('version') { should eq('1.9.5') }
end

describe package('git') do
  it { should be_installed }
  # its('version') { should eq('1.9.5') }
end

describe package('htop') do
  it { should be_installed }
  # its('version') { should eq('1.9.5') }
end

describe package('unzip') do
  it { should be_installed }
  # its('version') { should eq('1.9.5') }
end

describe package('zip') do
  it { should be_installed }
  # its('version') { should eq('1.9.5') }
end

# describe port(443) do
#   it { should be_listening }
#   its('protocols') {should include 'tcp'}
# end
